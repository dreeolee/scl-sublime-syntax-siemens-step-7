# SCL Sublime Syntax #

Here you can find the YAMLand the tmLanguage files used for the [PackageDev](https://github.com/SublimeText/PackageDev) package.

### Setup ###

* Follow [these](https://github.com/SublimeText/PackageDev) steps
* Follor [these](http://docs.sublimetext.info/en/latest/extensibility/syntaxdefs.html) steps
* Than you should get started with it.



### Workflow ###
* Please use the "default" Git Flow described [here.](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)





### Geetings ###

* DreeOlee